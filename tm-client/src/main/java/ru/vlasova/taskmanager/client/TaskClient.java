package ru.vlasova.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.model.dto.TaskDTO;

import java.util.List;

@FeignClient("task")
public interface TaskClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @PostMapping(value = "/task")
    TaskDTO createTask(TaskDTO task);

    @GetMapping(value = "/tasks")
    List<TaskDTO> getTasks();

    @GetMapping(value = "/task/{id}")
    TaskDTO getTask(@PathVariable("id") String id);

    @PutMapping(value = "/task/{id}")
    TaskDTO updateTask(@PathVariable("id") String id, TaskDTO task);

    @DeleteMapping(value = "/task/{id}")
    void deleteTask(@PathVariable("id") String id);

    @GetMapping(value = "/tasks/search/{keyword}")
    List<TaskDTO> searchTasks(@PathVariable(name="keyword") String keyword);

    @GetMapping(value = "/project/tasks/{id}")
    List<TaskDTO> getTasksByProject(@PathVariable("id") String id);

}

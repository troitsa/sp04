package ru.vlasova.taskmanager.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import ru.vlasova.taskmanager.client.ProjectClient;


@Configuration
@PropertySource("classpath:client.properties")
public class ApplicationConfig {

    final
    Environment env;

    @Autowired
    public ApplicationConfig(Environment env) {
        this.env = env;
    }

    @Bean
    ProjectClient simpleClient() {
        return ProjectClient.client(env.getProperty("url"));
    }

}

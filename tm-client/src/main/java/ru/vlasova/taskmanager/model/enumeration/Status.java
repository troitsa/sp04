package ru.vlasova.taskmanager.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Status {

    PLANNED("In planned"),
    PROGRESS("In progress"),
    READY("Ready");

    @Getter
    @NotNull
    private final String displayName;

}

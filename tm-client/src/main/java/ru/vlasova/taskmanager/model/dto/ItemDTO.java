package ru.vlasova.taskmanager.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.vlasova.taskmanager.model.enumeration.Status;

import java.util.Date;
import java.util.UUID;

public class ItemDTO extends AbstractDTO {
    @Setter
    @Getter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    @Nullable
    protected String userId;

    @Setter
    @Getter
    @Nullable
    protected String name = "";

    @Setter
    @Getter
    @Nullable
    protected String description = "";

    @Setter
    @Getter
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateCreate = new Date(System.currentTimeMillis());

    @Setter
    @Getter
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    @Setter
    @Getter
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @Setter
    @Getter
    @NotNull
    protected Status status = Status.PLANNED;

}

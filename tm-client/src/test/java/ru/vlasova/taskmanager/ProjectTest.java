package ru.vlasova.taskmanager;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.List;

public class ProjectTest extends AbstractTest {

    @Test
    public void createProject() {
        @NotNull final ProjectDTO project = newProject("1");
        projectClient.createProject(project);
        Assert.assertNotNull(projectClient.getProject(project.getId()));
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void getProjects() {
        @NotNull final ProjectDTO project1 = newProject("1");
        @NotNull final ProjectDTO project2 = newProject("2");
        @NotNull final ProjectDTO project3 = newProject("2");
        projectClient.createProject(project1);
        projectClient.createProject(project2);
        projectClient.createProject(project3);
        projectClient.deleteProject(project1.getId());
        projectClient.deleteProject(project2.getId());
        projectClient.deleteProject(project3.getId());
    }

    @Test
    public void getProject() {
        @NotNull final ProjectDTO project = newProject("1");
        projectClient.createProject(project);
        Assert.assertEquals(projectClient.getProject(project.getId()), project);
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void updateProject() {
        @NotNull final ProjectDTO project = newProject("1");
        projectClient.createProject(project);
        project.setName("Test-2");
        projectClient.updateProject(project.getId(), project);
        Assert.assertEquals(projectClient.getProject(project.getId()).getName(), project.getName());
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void deleteProject() {
        @NotNull final ProjectDTO project = newProject("1");
        projectClient.createProject(project);
        projectClient.deleteProject(project.getId());
        try {
            projectClient.getProject(project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    public void searchProjects() {
        @NotNull final ProjectDTO project1 = newProject("1");
        project1.setName("tasksk");
        @NotNull final ProjectDTO project2 = newProject("2");
        project2.setName("tasksk");
        @NotNull final ProjectDTO project3 = newProject("2");
        projectClient.createProject(project1);
        projectClient.createProject(project2);
        projectClient.createProject(project3);
        @NotNull final List<ProjectDTO> projectList = projectClient.searchProjects("tasksk");
        Assert.assertEquals(projectList.size(), 2);
        projectClient.deleteProject(project1.getId());
        projectClient.deleteProject(project2.getId());
        projectClient.deleteProject(project3.getId());
    }

}

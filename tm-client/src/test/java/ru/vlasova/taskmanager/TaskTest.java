package ru.vlasova.taskmanager;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.List;

public class TaskTest extends AbstractTest {

    @Test
    public void createTask() {
        @NotNull final TaskDTO task = newTask("1");
        taskClient.createTask(task);
        Assert.assertNotNull(taskClient.getTask(task.getId()));
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void getTasks() {
        @NotNull final TaskDTO task1 = newTask("1");
        @NotNull final TaskDTO task2 = newTask("2");
        @NotNull final TaskDTO task3 = newTask("2");
        taskClient.createTask(task1);
        taskClient.createTask(task2);
        taskClient.createTask(task3);
        taskClient.deleteTask(task1.getId());
        taskClient.deleteTask(task2.getId());
        taskClient.deleteTask(task3.getId());
    }

    @Test
    public void getTask() {
        @NotNull final TaskDTO task = newTask("1");
        taskClient.createTask(task);
        Assert.assertEquals(taskClient.getTask(task.getId()), task);
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void updateTask() {
        @NotNull final TaskDTO task = newTask("1");
        taskClient.createTask(task);
        task.setName("Test-2");
        taskClient.updateTask(task.getId(), task);
        Assert.assertEquals(taskClient.getTask(task.getId()).getName(), task.getName());
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void deleteTask() {
        @NotNull final TaskDTO task = newTask("1");
        taskClient.createTask(task);
        taskClient.deleteTask(task.getId());
        try {
            taskClient.getTask(task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("404"));
        }
    }

    @Test
    public void searchTasks() {
        @NotNull final TaskDTO task1 = newTask("1");
        task1.setName("tasksk");
        @NotNull final TaskDTO task2 = newTask("2");
        task2.setName("tasksk");
        @NotNull final TaskDTO task3 = newTask("3");
        taskClient.createTask(task1);
        taskClient.createTask(task2);
        taskClient.createTask(task3);
        @NotNull final List<TaskDTO> taskList = taskClient.searchTasks("tasksk");
        Assert.assertEquals(taskList.size(), 2);
        taskClient.deleteTask(task1.getId());
        taskClient.deleteTask(task2.getId());
        taskClient.deleteTask(task3.getId());
    }

    @Test
    public void getTasksByProjectId() {
        @NotNull final ProjectDTO project = newProject("1");
        projectClient.createProject(project);
        @NotNull final TaskDTO task1 = newTask("1");
        task1.setProjectId(project.getId());
        @NotNull final TaskDTO task2 = newTask("2");
        task2.setProjectId(project.getId());
        @NotNull final TaskDTO task3 = newTask("3");
        taskClient.createTask(task1);
        taskClient.createTask(task2);
        taskClient.createTask(task3);
        @NotNull final List<TaskDTO> taskList = taskClient.getTasksByProject(project.getId());
        Assert.assertEquals(taskList.size(), 2);
        projectClient.deleteProject(project.getId());
    }

    private TaskDTO newTask(@NotNull final String s) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("TestTask" + s);
        task.setDescription("Description 123");
        task.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        task.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return task;
    }

}

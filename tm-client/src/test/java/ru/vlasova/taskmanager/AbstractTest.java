package ru.vlasova.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.taskmanager.client.ProjectClient;
import ru.vlasova.taskmanager.client.TaskClient;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public class AbstractTest {

    @NotNull
    final ProjectClient projectClient = ProjectClient.client("http://localhost:8080");

    @NotNull
    final TaskClient taskClient = TaskClient.client("http://localhost:8080");

    protected ProjectDTO newProject(@NotNull final String s) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("TestProject" + s);
        project.setDescription("Description 123");
        project.setDateStart(toDateFormat("2020-02-13T18:51:09.840Z"));
        project.setDateFinish(toDateFormat("2020-02-23T18:51:09.840Z"));
        return project;
    }

    protected Date toDateFormat(@NotNull final String date) {
        TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(date);
        Instant i = Instant.from(ta);
        return Date.from(i);
    }
}

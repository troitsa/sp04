package ru.vlasova.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.entity.Project;

import java.util.Arrays;
import java.util.List;

@Controller
public class ProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @RequestMapping("index")
    public String index() {
        return "index";
    }

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/project_list")
    public ModelAndView projectList() {
        @Nullable final List<Project> projectList = projectService.findAll();
        @NotNull final ModelAndView mav = new ModelAndView("project_list");
        mav.addObject("projectList", projectList);
        return mav;
    }

    @RequestMapping(value = "/new_project")
    public String newProjectForm(ModelMap model) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        model.addAttribute("project", project);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        model.addAttribute("statusList", statusList);
        return "new_project";
    }

    @RequestMapping(value = "project_save", method = RequestMethod.POST)
    public String saveProject(@Nullable @ModelAttribute final ProjectDTO project) {
        projectService.merge(projectService.toProject(project));
        return "redirect:/project_list";
    }

    @RequestMapping(value = "/edit_project")
    public ModelAndView editProjectForm(@NotNull @RequestParam final String id) {
        @NotNull final ModelAndView mav = new ModelAndView("edit_project");
        @Nullable final ProjectDTO project = projectService.toProjectDTO(projectService.findOne(id));
        mav.addObject("project", project);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        mav.addObject("statusList", statusList);
        return mav;
    }

    @RequestMapping(value = "/delete_project")
    public String deleteProjectForm(@NotNull @RequestParam final String id) {
        projectService.remove(id);
        return "redirect:/project_list";
    }

    @RequestMapping(value = "search_project")
    public ModelAndView search(@NotNull @RequestParam final String keyword) {
        @Nullable final List<Project> result = projectService.search(keyword);
        @NotNull final ModelAndView mav = new ModelAndView("search_project");
        mav.addObject("result", result);
        return mav;
    }

}

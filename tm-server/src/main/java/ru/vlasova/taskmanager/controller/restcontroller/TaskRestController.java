package ru.vlasova.taskmanager.controller.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskRestController {

    private final ITaskService taskService;

    @Autowired
    public TaskRestController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/task/{id}")
    public ResponseEntity<TaskDTO> getTask(@PathVariable("id") String id) {
        @Nullable final TaskDTO task = taskService.toTaskDTO(taskService.findOne(id));
        return task != null
                ? new ResponseEntity<>(task, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/task")
    public ResponseEntity<?> createTask(@RequestBody TaskDTO task) {
        taskService.merge(taskService.toTask(task));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/tasks")
    public ResponseEntity<List<TaskDTO>> getTasks() {
        @NotNull final List<TaskDTO> tasks = taskService
                .findAll()
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/task/{id}")
    public ResponseEntity<?> updateTask(@PathVariable(name = "id") String id, @RequestBody TaskDTO task) {
        if (taskService.findOne(id) != null) {
            taskService.merge(taskService.toTask(task));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/task/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable(name = "id") String id) {
        taskService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/tasks/search/{keyword}")
    public ResponseEntity<List<TaskDTO>> searchTasks(@PathVariable(name = "keyword") String keyword) {
        @NotNull final List<TaskDTO> tasks = taskService
                .search(keyword)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/tasks/byproject/{id}")
    public ResponseEntity<List<TaskDTO>> tasksByProject(@PathVariable(name = "id") String id) {
        @NotNull final List<TaskDTO> tasks = taskService
                .getTasksByProjectId(id)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/project/tasks/{id}")
    public ResponseEntity<List<TaskDTO>> getTasksByProject(@PathVariable("id") String id) {
        @Nullable final List<TaskDTO> tasks = taskService
                .getTasksByProjectId(id)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

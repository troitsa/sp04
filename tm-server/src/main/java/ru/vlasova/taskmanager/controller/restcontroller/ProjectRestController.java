package ru.vlasova.taskmanager.controller.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.entity.Project;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectRestController {

    private final IProjectService projectService;

    @Autowired
    public ProjectRestController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/project/{id}")
    public ResponseEntity<ProjectDTO> getProject(@PathVariable("id") String id) {
        @Nullable final ProjectDTO projectDTO = projectService.toProjectDTO(projectService.findOne(id));
        return projectDTO != null
                ? new ResponseEntity<>(projectDTO, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/project")
    public ResponseEntity<?> createProject(@RequestBody Project project) {
        projectService.merge(project);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/projects")
    public ResponseEntity<List<ProjectDTO>> getProjects() {
        @NotNull final List<ProjectDTO> projects = projectService
                .findAll()
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        return !projects.isEmpty()
                ? new ResponseEntity<>(projects, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/project/{id}")
    public ResponseEntity<?> updateProject(@PathVariable(name = "id") String id, @RequestBody ProjectDTO project) {
        if (projectService.findOne(id) != null) {
            projectService.merge(projectService.toProject(project));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/project/{id}")
    public ResponseEntity<?> deleteProject(@PathVariable(name = "id") String id) {
        projectService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/projects/search/{keyword}")
    public ResponseEntity<List<ProjectDTO>> searchProjects(@PathVariable(name = "keyword") String keyword) {
        @Nullable final List<ProjectDTO> projects = projectService
                .search(keyword)
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        return !projects.isEmpty()
                ? new ResponseEntity<>(projects, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

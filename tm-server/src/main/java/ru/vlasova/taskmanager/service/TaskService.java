package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.repository.TaskRepository;

import java.util.Date;
import java.util.List;

@Service("taskService")
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    @Qualifier(value = "taskRepository")
    private TaskRepository repository;

    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String projectId) {
        if (projectId == null) return null;
        @NotNull final List<Task> taskList = repository.findAllByProjectId(projectId);
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final String name,
                       @Nullable final String description, @Nullable final Date dateStart,
                       @Nullable final Date dateFinish, @Nullable final String project,
                       @Nullable final String status) {
        final boolean checkGeneral = isValid(name, description);
        if (!checkGeneral) return null;
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        task.setProject(projectService.findOne(project));
        task.setStatus(Status.valueOf(status));
        return task;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String searchString) {
        if (searchString == null || searchString.isEmpty()) return null;
        @NotNull final List<Task> taskList = repository.
                findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(searchString, searchString);
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final List<Task> taskList = (List<Task>) repository.findAll();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = repository.findById(id).orElse(null);
        return task;
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public @Nullable List<Task> sortTask(@Nullable final String sortMode) {
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return repository.findAllByOrderByDateCreate();
                case ("2"):
                    return repository.findAllByOrderByDateStart();
                case ("3"):
                    return repository.findAllByOrderByDateFinish();
                case ("4"):
                    return repository.findAllByOrderByStatusAsc();
            }
        }
        return repository.findAllByOrderByNameAsc();
    }

    @Nullable
    public Task toTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        @Nullable final Task task = new Task();
        task.setId(taskDTO.getId());
        if(taskDTO.getProjectId()!=null)
            task.setProject(projectService.findOne(taskDTO.getProjectId()));
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateCreate(taskDTO.getDateCreate());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setStatus(taskDTO.getStatus());
        return task;
    }

    @Nullable
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        if(task.getProject()!=null)
            taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

}

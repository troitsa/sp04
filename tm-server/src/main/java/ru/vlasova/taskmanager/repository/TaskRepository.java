package ru.vlasova.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.taskmanager.model.entity.Task;

import java.util.List;

@Repository("taskRepository")
public interface TaskRepository extends CrudRepository<Task, String> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String searchString1, String searchString2);

    List<Task> findAllByOrderByDateCreate();

    List<Task> findAllByOrderByDateStart();

    List<Task> findAllByOrderByDateFinish();

    List<Task> findAllByOrderByStatusAsc();

    List<Task> findAllByOrderByNameAsc();

}

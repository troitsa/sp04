package ru.vlasova.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.taskmanager.model.entity.Project;

import java.util.List;

@Repository("projectRepository")
public interface ProjectRepository extends CrudRepository<Project, String> {

    List<Project> findAllByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String searchString1, String searchString2);

    List<Project> findAllByOrderByDateCreate();

    List<Project> findAllByOrderByDateStart();

    List<Project> findAllByOrderByDateFinish();

    List<Project> findAllByOrderByStatusAsc();

    List<Project> findAllByOrderByNameAsc();
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="header.jsp" %>

<h1>Результаты поиска</h1>

<table class="table table-striped">
    <col width="50px">
    <col>
    <col>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <c:forEach items="${result}" var="task">
        <tr>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
        </tr>
    </c:forEach>
</table>

<%@ include file="footer.jsp" %>